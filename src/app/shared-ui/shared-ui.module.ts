import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { HttpErrorHandler } from '../common/services/http-error-handler/http-error-handler.service';
import { MessageService } from '../common/services/message/message.service';
import { AgmCoreModule } from '@agm/core';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCSme-ssBMx7P8521qElJ6Jcpilp7MbEWI',
    }),
    SharedImportsModule,
    CommonModule,
  ],
  providers: [HttpErrorHandler, MessageService],
  exports: [HomeComponent],
})
export class SharedUIModule {}
