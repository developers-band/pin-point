import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  HandleError,
  HttpErrorHandler,
} from './common/services/http-error-handler/http-error-handler.service';
import { Observable } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { API_PREFIX } from './constants/storage';

@Injectable()
export class AppService {
  messageUrl = `${API_PREFIX}/info`; // URL to web api
  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AppService');
  }

  /** GET message from the server */
}
